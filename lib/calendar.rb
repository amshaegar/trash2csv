require 'uri'
require 'net/http'
require 'nokogiri'

class Calendar
  def initialize(url)
    @uri = URI.parse url
    @req = Net::HTTP::Post.new @uri
    @index = 0
  end

  def next
    i = @index += 1
    @req.set_form_data strasse: i, anzeigen: 'anzeigen'

    res = Net::HTTP.start @uri.hostname, @uri.port, use_ssl: true do |http|
      http.request @req
    end

    page = Nokogiri::HTML res.body

    [date(page), street(page)]
  end

  private

  def date(page)
    trash = page.css('tr').detect do |tr|
      set = tr.css %q(img[title='Sperrmüllabholung'])
      !set.empty?
    end
    trash.css('td').last.css('b').text[/\d{1,2}\.\d{1,2}\.\d{4}/]
  end

  def street(page)
    street = page.css('h1').detect do |h1|
      !h1.css('a').empty?
    end
    street.css('a').text.strip
  end
end
