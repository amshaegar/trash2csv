require 'rubygems'
require 'calendar'
require 'date'
require 'csv'
require 'pp'

class Trash2CSV
  def self.run(out)
    cal = Calendar.new 'https://web3.karlsruhe.de/service/abfall/akal/akal.php?von=A&bis=['

    lines = []
    loop do
      begin
        l = cal.next
        break if l.last.empty?
        next if l.first.nil?
        puts l.last
        lines << l
      rescue => e
        pp e
        break
      end
    end

    lines.sort_by! do |l|
      Date.parse l.first
    end

    CSV.open(out, 'w') do |csv|
      lines.each do |l|
        csv << l
      end
    end
  end
end
